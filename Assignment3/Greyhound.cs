﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment3
{
    class Greyhound
    {
        /// <summary>
        /// The starting position of the greyhound
        /// </summary>
        private double _startPosition;

        /// <summary>
        /// The location of the Greyhound on the racetrack
        /// </summary>
        private int _location;

        /// <summary>
        /// The length of the Racetrack
        /// </summary>
        private int _raceTrackLength;

        /// <summary>
        /// Represents the racehound image UI element
        /// </summary>
        private Image _uiRaceHound;

        /// <summary>
        /// Represents the random distance the greyhound will travel
        /// </summary>
        private Random _randomizer;

        /// <summary>
        /// Greyhound construtor
        /// </summary>
        public Greyhound(Image uiRaceHound, double startPosition, int raceTrackLength)
        {
            //Initialize the start position for the Greyhound
            _startPosition = startPosition;

            //Initialize the race track length 
            _raceTrackLength = raceTrackLength;

            //Initialize the location
            _location = 0;

            //Initialize the randomizer variable
            _randomizer = new Random();

            //Initialize the racehound image
            _uiRaceHound = uiRaceHound;
        }

        //Define the read-only property for start position
        public double StartPosition
        {
            get { return _startPosition; }
        }

        //Define the read and write properties for the location 
        public int Location
        {
            get { return _location; }
            set { _location = value; }
        }

        //Define the read-only properties for track length
        public int RaceTrackLength
        {
            get { return _raceTrackLength; }
        }

        //Define the read and write properties of the Randomizer
        public Random Ranomizer
        {
            get { return _randomizer; }
            set { _randomizer = value; }
        }

        /// <summary>
        /// Moves and updates the position of the Greyhound
        /// </summary>
        /// <returns></returns>
        public bool Run()
        {
            
            //Move forward either 1, 2, 3 or 4 spaces at a time

            //Update the position of Greyhound like this:
            //Greyhound.Left = Starting + Location;

            //return true if greyhound won 
            return false;
        }

        /// <summary>
        /// Resets Greyhound starting position
        /// </summary>
        public void TakeStartingPosition()
        {
            //Reset location to 0 and Greyhound to starting position
        }
    }
}
