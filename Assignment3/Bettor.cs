﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment3
{
    class Bettor
    {
        /// <summary>
        /// Bettor's name
        /// </summary>
        private string _name;

        /// <summary>
        /// Bettor's cash
        /// </summary>
        private int _cash;

        /// <summary>
        /// Represents the Better's bet on the Greyhound
        /// </summary>
        private Bet _bet;

        /// <summary>
        /// Represents the radio button UI element
        /// </summary>
        private RadioButton _uiBetter;

        /// <summary>
        /// Represents the Text block UI element
        /// </summary>
        private TextBlock _uiBetDesc;

        /// <summary>
        /// Better constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cash"></param>
        /// <param name="bet"></param>
        /// <param name="uiBetter"></param>
        /// <param name="uiBetDesc"></param>
        public Bettor(string name, int cash, Bet bet, RadioButton uiBetter, TextBlock uiBetDesc)
        {
            //Initialize the name of the bettor
            _name = name;

            //Initialize the cash of the bettor
            _cash = cash;

            //Initialize the bet object
            _bet = bet;

            //Initialize the better UI element
            _uiBetter = uiBetter;

            //Initialize the bet description UI element
            _uiBetDesc = uiBetDesc;
        }

        //Define the read and write properties of the name
        public string Name
        {
            get { return _name; }
        }

        //Define the read and write properties of the cash 
        public int Cash
        {
            get { return _cash; }
            set { _cash = value; }
        }

        //Define the read-only properties for bet
        public Bet BetVar
        {
            get { return _bet; }
        }

        //Define the read-only properties for UI radio button
        public RadioButton UiBetter
        {
            get { return _uiBetter; }
        }

        //Define the read-only properties for the UI text block
        public TextBlock UiBetDesc
        {
            get { return _uiBetDesc; }
        }

        /// <summary>
        /// An indicator that the bettor has placed his/her bet
        /// </summary>
        public bool HasPlacedBet
        {
            get { return true; }
        }

        /// <summary>
        /// Places a bet amount on selected greyhound
        /// </summary>
        /// <param name="betAmount"></param>
        /// <param name="houndToWin"></param>
        /// <returns></returns>
        public bool PlaceBet(int betAmount, int houndToWin)
        {
            if (betAmount > Cash)
            {
                return false;
            }
            else
            {
                //Create new bet object
                _bet = new Bet(betAmount, houndToWin, this);

                //Update textbox
                UpdateLabels();

                return true;
            }            
        }

        /// <summary>
        /// Clears the bet made on the Greyhound
        /// </summary>
        public void ClearBet()
        {

        }

        /// <summary>
        /// Collects all the bet amounts made on each Greyhound 
        /// </summary>
        /// <param name="winnerHound"></param>
        public void Collect(int winnerHound)
        {

        }

        /// <summary>
        /// Updates the textbox 
        /// </summary>
        public void UpdateLabels()
        {            
            //get bet description
            if(_bet == null)
            {
                _uiBetDesc.Text = $"{Name} hasn't placed a bet";
            }
            else
            {
                //set textbox to bets description
                _uiBetDesc.Text = _bet.GetDescription();
            }         

            //set radiobutton to show cash
            _uiBetter.Content = $"{Name} has {Cash} bucks";

        }
    }
}
