﻿//using Assignment3.Assets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assignment3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Link presentation layer with the list of the Greyhound objects
        /// </summary>
        private List<Greyhound> _raceHoundList;

        /// <summary>
        /// Link presentation layer with the list of the Better objects
        /// </summary>
        private List<Bettor> _betterList;
        
        /// <summary>
        /// Represents the selected better linked presentation layer 
        /// </summary>
        private Bettor _crtSelBetter;

        public MainPage()
        {
            this.InitializeComponent();

            //Initialize the greyhound list
            _raceHoundList = new List<Greyhound>();

            //Initialize the better list
            _betterList = new List<Bettor>();

            //Create Bettors
            CreateBetters();

            //Create Racehounds
            CreateRaceHounds();
        }

        /// <summary>
        /// Creates the Greyhound objects
        /// </summary>
        private void CreateRaceHounds()
        {
            //get the start position and track length to pass to greyhound objects
            double startPos = raceDog1.Width;
            int trackLength = Convert.ToInt32(TrackPic.Width - raceDog1.Width);

            //create the first greyhound object and add it to list of greyhounds
            Greyhound greyHound1 = new Greyhound(raceDog1, startPos, trackLength);
            _raceHoundList.Add(greyHound1);

            //create the second greyhound object and add it to the list of greyhounds
            Greyhound greyHound2 = new Greyhound(raceDog2, startPos, trackLength);
            _raceHoundList.Add(greyHound2);

            //create the third greyhound object and add it to the list of greyhounds
            Greyhound greyHound3 = new Greyhound(raceDog3, startPos, trackLength);
            _raceHoundList.Add(greyHound3);

            //create the fourth greyhound object and add it to the list of greyhounds
            Greyhound greyHound4 = new Greyhound(raceDog4, startPos, trackLength);
            _raceHoundList.Add(greyHound4);
        }

        /// <summary>
        /// Creates the Better objects
        /// </summary>
        private void CreateBetters()
        {
            //create the first better and add it to the list of bettors
            Bettor better1 = new Bettor("Joe", 50, null, joeRadioButton, joeTextBlock);
            _betterList.Insert(0, better1);

            //create the second better and add it to the list of bettors
            Bettor better2 = new Bettor("Bob", 75, null, bobRadioButton, bobTextBlock);
            _betterList.Insert(1, better2);
                
            //create the third better and add it to the list of bettors
            Bettor better3 = new Bettor("Anna", 45, null, annaRadioButton, annaTextBlock);
            _betterList.Insert(2, better3);

            //update labels
            better1.UpdateLabels();
            better2.UpdateLabels();
            better3.UpdateLabels();
        }

        private void JoeChecked(object sender, RoutedEventArgs e)
        {
            //change {Selected} text block to Joe
            textBlock1.Text = "Joe";
        }

        private void BobChecked(object sender, RoutedEventArgs e)
        {
            //change {Selected} text block to Bob
            textBlock1.Text = "Bob";
        }

        private void AnnaChecked(object sender, RoutedEventArgs e)
        {
            //change {Selected} text block to Anna
            textBlock1.Text = "Anna";
        }

        private void OnPlaceBet(object sender, RoutedEventArgs e)
        {
            if (textBlock1.Text == "Joe")
            {
                _betterList[0].PlaceBet(Convert.ToInt32(amount.Text), Convert.ToInt32(dogNum.Text));
            }
            else if(textBlock1.Text == "Bob")
            {
                _betterList[1].PlaceBet(Convert.ToInt32(amount.Text), Convert.ToInt32(dogNum.Text));
            }
            else if (textBlock1.Text == "Anna")
            {
                _betterList[2].PlaceBet(Convert.ToInt32(amount.Text), Convert.ToInt32(dogNum.Text));
            }
        }
    }
}
