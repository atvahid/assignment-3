﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Bet
    {
        /// <summary>
        /// The bet amount
        /// </summary>
        private int _amount;

        /// <summary>
        /// The Racehound number
        /// </summary>
        private int _raceHound;

        /// <summary>
        /// Represents the bettor
        /// </summary>
        private Bettor _bettor;

        /// <summary>
        /// Bet constructor
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="raceHound"></param>
        /// <param name="better"></param>
        public Bet(int amount, int raceHound, Bettor better)
        {
            //Initialize the bet amount
            _amount = amount;

            //Initialize the racehound number
            _raceHound = raceHound;

            //Initialize the bettor
            _bettor = better;
        }

        //Define read and write properties for amount
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        //Define read and write properties of Racehound
        public int RaceHound
        {
            get { return _raceHound; }
            set { _raceHound = value; }
        }

        //Define read-only properties for better
        public Bettor BetterGuy
        {
            get { return _bettor; }
        }

        /// <summary>
        /// the amount to be payed if the better won, or deducted if the better lost
        /// </summary>
        /// <param name="winner"></param>
        /// <returns></returns>
        public int PayOut(int winner)
        {
            return 0;
        }

        /// <summary>
        /// Returns a string that says who placed the bet, how much cash was bet, 
        /// and which dog he/she bet on
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            //return a string that displays who placed the bet
            if(Amount == 0)
            {
                //if the amount is zero, no bet was placed
                return $"{_bettor.Name} hasn't placed a bet";
            }
            else
            {
                return $"{_bettor.Name} bets {Amount} on dog {RaceHound}";
            }
            
        }
    }
}
